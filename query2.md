


```sql
PREFIX dcterms: <http://purl.org/dc/terms/>
SELECT DISTINCT ?s ?mol_weight ?o
FROM <http://nikkaji.biosciencedbc.jp/main>
WHERE {
  ?s ?p ?o.
  ?o bif:contains '"Selenazo"'.
  ?s <http://vocab.jst.go.jp/terms/sti#molar-weight> ?mol_weight.
  FILTER (?mol_weight > 333)
}
```
