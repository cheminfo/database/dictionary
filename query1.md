

```
PREFIX dcterms: <http://purl.org/dc/terms/>
SELECT ?p ?o
FROM <http://nikkaji.biosciencedbc.jp/main>
WHERE {
  { 
    ?s ?p ?o.
    ?s dcterms:identifier "J2.171.076H".
  }
}

OFFSET 0
LIMIT 100
```




```
http://www.w3.org/1999/02/22-rdf-syntax-ns#type	http://vocab.jst.go.jp/terms/sti#Chemical
http://www.w3.org/2000/01/rdf-schema#label	"2-セレナナフト[2,1-d]セレナゾール-2-イウム クロリド"@ja
http://www.w3.org/2000/01/rdf-schema#label	"2-Selenanaphtho[2,1-d]selenazole-2-ium chloride"@en
http://purl.org/dc/terms/identifier	"J2.171.076H"
http://vocab.jst.go.jp/terms/vlist#systematic-name	nodeID://b139815834
http://vocab.jst.go.jp/terms/vlist#common-name	nodeID://b139815836
http://vocab.jst.go.jp/terms/sti#chem-formula	"CL^C10-H6-N-SE2"
http://vocab.jst.go.jp/terms/sti#chem-formula	"Cl C<sub>10</sub>H<sub>6</sub>NSe<sub>2</sub>"^^<http://www.w3.org/2000/01/rdf-schema#Literal>
http://vocab.jst.go.jp/terms/sti#common-name	"2-Selenanaphtho[2,1-d]selenazole-2-ium chloride"@en
http://vocab.jst.go.jp/terms/sti#molar-weight	333.538
http://vocab.jst.go.jp/terms/sti#nikkaji-ID-registry	"02171076H"
http://vocab.jst.go.jp/terms/sti#nikkaji-number	"J2.171.076H"
http://vocab.jst.go.jp/terms/sti#systematic-name	"2-セレナナフト[2,1-d]セレナゾール-2-イウム クロリド"@ja
http://www.w3.org/2008/05/skos-xl#altLabel	nodeID://b139815842
http://vocab.jst.go.jp/terms/sti#component-nikkaji-number	nodeID://b139815838
http://vocab.jst.go.jp/terms/sti#material-type	"X"
```


